# Criar as disciplinas do curso - Done
# Criar o sistema de avaliação para cada disciplina - Done
# Registar as classificações obtidas - Done
# Verificar o seu estado de avaliação - Done
# Obter informações úteis, tais como: datas de avaliação; classificação mínima necessária na última prova
# Gráficos informativos
#
# Curso - Done
# Lista de disciplinas do primeiro ano, primeiro semestre - Done
# Docentes das disciplinas - Done
# Em cada disciplina: Elementos de avaliação, sua ponderação e datas - Done
# Classificações obtidas - Done
#
# Validações
# - Validar datas - Done
# - Validar que a ponderação total não é maior que 100%
# - Validar que o input do utilizador é válido, senão repete a opção


# Resultados a obter
# Afixar a tabela de avaliação de uma dada disciplina e o nome do docente respectivo; - Função feita
# Consultar as próximas datas de avaliação (Trabalhos ou Testes); - Função feita
# Quando só faltar um elemento de avaliação, calcular qual a nota mínima necessária para obter aprovação com 10 valores. Considerar também a hipótese de desejar obter uma determinada nota final (indicada via teclado), e para esta, calcular qual a nota mínima necessária para obter aprovação com esta determinada nota final; #feita parcialmente
# Apresentar um gráfico de barras verticais com as classificações obtidas em uma disciplina, representando no eixo dos X - os elementos de avaliação, e no eixo dos Y – as notas obtidas; #feito
# Apresentar um gráfico de barras horizontal que compare as classificações finais de todas as disciplinas, representando no eixo dos X – as notas finais e no eixo dos Y – os nomes das disciplinas. feito

#importar modulos
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, date
import pickle as pk

#Inicializar variaveis
curso = "Engenharia Informática"
disciplinas = []
docentes = []
elementos = []
ponderacoes = []
datas = []
notas = []
megaArray = [disciplinas,docentes,elementos,ponderacoes,datas,notas]

# avaliacao = [disciplinas]
# elementos_avaliacao = []
ponderacaoNota = [disciplinas]


# megaArray = [['AP','EDC'], [['Marco', 'Enes'],['Rui Neves']], [['Teste #1', 'Teste #2'],['Teste EDC']], [[50, 50],["100"]], [['01/02/18', '02/01/18'],['03/01/18']], [[15],[' ']]]
# megaArray = [
#             ["AP","EDC","SD"], #Disciplinas
#             [["Marco Costa","Paulo Enes"],["Rui Neves"],["Silvestre","João"]], #Docentes
#             [["Teste#1","Teste#2","Trabalho","Presenças"],["Teste#1","Teste#2","Trabalho","Presenças"],["Teste#1","Teste#2","Trabalho","Presenças"]], #Elementos
#             [["30%","30%","30%","5%"],["25%","25%","45%","5%"],["25%","25%","45%","5%"]], #Ponderações
#             [["20/01/17","21/02/18","05/01/18","03/01/18"],["20/01/17","21/02/18","05/01/18","03/01/18"],["20/01/17","21/02/18","05/01/18",""]], #Datas
#             [["10","8","20","20"],["8","10","12","20"],["11","12","14","10","12"]] #Notas
#             ]


""" ======== """
""" GRÁFICOS """
""" ======== """
def gravaTxt(lista):
    if len(lista) == 0:
        print("Lista vazia")
    else:
        nome_fx = "testeGraf.txt"
        fx = open(nome_fx, 'w+')
        for disciplina in range(len(lista[0])):
            fx.write("Disciplina" + "\n")
            fx.write(lista[0][disciplina] + "\n")
            for docente in range(len(lista[1][disciplina])):
                fx.write("Docente:" + "\n")
                fx.write(lista[1][disciplina][docente] + "\n")
            for elemento in range(len(lista[2][disciplina])):
                fx.write("Elemento de avaliação" + "\n")
                fx.write(lista[2][disciplina][elemento] + "\n")
            for ponderacao in range(len(lista[3][disciplina])):
                fx.write("Ponderação:" + "\n")
                fx.write(str(lista[3][disciplina][ponderacao]) + "\n")
            for data in range(len(lista[4][disciplina])):
                fx.write("Datas:" + "\n")
                fx.write(lista[4][disciplina][data] + "\n")
        fx.close()

def gravaBin(lista):
    if len(lista) == 0:
        print("Lista vazia")
    else:
        with open("bin.txt", "wb") as file:
            pk.dump(lista, file)

def leTxt():
    nome_fx = "testeGraf.txt"
    fx = open(nome_fx, 'r')
    for linha in fx.readlines():
        print(linha)
    fx.close()

def leBin():
    with open("bin.txt", "rb") as file:
        global megaArray
        megaArray = pk.load(file)


def grafico (notas, elementos):
    bar_width = 0.50
    opacity = 0.4
    index = np.arange(len(notas))
    plt.bar(index, notas, bar_width, alpha=opacity, color='b')

    plt.xlabel('Elementos')
    plt.ylabel('Valores')
    plt.title('Notas por Elemento',style='italic')
    plt.xticks(index, elementos)
    #plt.legend()
    plt.xticks(rotation=45)
    for i,n in enumerate(notas):
        plt.text( i , n, n, ha='center', va='bottom')
    #plt.grid(True)
    plt.show()

def grafNotasFinais(notas, disciplinas):
    if len(notas) == 1:
        print("Só tem uma disciplina preenchida")
    else:
        bar_width = 0.50
        opacity = 0.4
        index = np.arange(len(disciplinas))
        plt.barh(index, notas, bar_width, alpha=opacity, color='b')
        plt.yticks(index, disciplinas)
        plt.yticks(rotation=45)
        plt.xlabel('Notas')
        plt.ylabel('Disciplinas')
        plt.title('Notas Finais',style='italic')
        #plt.grid(True)
        for i,n in enumerate(notas):
            plt.text( n, i, n, ha='left', va='center')
        plt.show()

""" ========================= """
""" Média final e Nota miníma """
""" ========================= """

def calcularMedia(ponderacao,classificacao):
    innerArray = [ponderacao,classificacao] #trabalhamos com este
    # arrayDummy = [["30","20","25","25"],["10","7","11","16"]]
    # mediaEstatica = ((30*10)+(7*20)+(11*25)+(16*25))/100
    multiplicacoes = [] #guardo as multiplicacoes
    soma = 0 #faco soma dos valores do array aqui
    for i in range(len(innerArray[0])):
        if isinstance(innerArray[1][i], str): #valida se é string
            pass
        else:
            multiplicacoes.append(int(innerArray[0][i]) * int(innerArray[1][i]))
    for number in range(len(multiplicacoes)):
        soma = soma + int(multiplicacoes[number])
    return soma / 100

def calcularNotaMinima(ponderacaoNota,indiceDisciplina):
    ponderacao =  ponderacaoNota[1] #megaArray[0][3]
    classificacao = ponderacaoNota[2] #megaArray[0][5]

    tamanhoArray = len(ponderacao) - len(classificacao)
    if tamanhoArray == 1:
        classificacao.append(0)
        nota = calcularMedia(ponderacao,classificacao)
        if nota <= 10:
            notaDesejada = 10
            notaAtual = nota
            ponderacaoElemento = float(0.01 * ponderacao[-1])
            #saquei daqui a formula https://www.rapidtables.com/calc/grade/final-grade-calculator.html
            classificacaoNecessaria = (notaDesejada - ((1 - ponderacaoElemento) * notaAtual)) / ponderacaoElemento
            print("A media atual é \x1b[6;30;47m {} \x1b[0m".format(nota))
            print("Nota minima para 10 é \x1b[6;30;47m {} \x1b[0m".format(classificacaoNecessaria))

        else:
            print("A media é \x1b[6;30;47m {} \x1b[0m".format(nota))
    else:
        print("A sua media é \x1b[6;30;47m {} \x1b[0m".format(calcularMedia(ponderacao,classificacao)))

""" ======================= """
""" Tabelas de visualização """
""" ======================= """

def verProximasDatas(matriz,indiceDisciplina): #falta fazer
    #tODO
    innerDatas = []
    innerPond = []
    innerElemento = []
    innerArray = [matriz[0][indiceDisciplina],innerElemento,innerPond,innerDatas]
    for data in range(len(matriz[4][indiceDisciplina])):
        # print(matriz[4][indiceDisciplina][data])
        # print(datetime.now().strftime("%d/%m/%y"))
        #print(matriz[4][indiceDisciplina][data] >= datetime.now().strftime("%d/%m/%y"))
        if (matriz[4][indiceDisciplina][data] <= datetime.now().strftime("%d/%m/%y")):
            innerElemento.append(matriz[2][indiceDisciplina][data])
            innerPond.append(matriz[3][indiceDisciplina][data])
            innerDatas.append(matriz[4][indiceDisciplina][data])

    for disciplina in range(1): #Vai iterar pela array das Disciplinas
        #print(matriz[0][disciplina]) #Print das Disciplinas
        print("\x1b[44m Disciplina \x1b[0m|")
        print('\x1b[44m {0:10} \x1b[0m'.format(innerArray[0]), end='|')
        print('\n')
        # print("\x1b[6;30;47m Elementos de Avaliação, Ponderações, Datas e Classificações \x1b[0m")
        # print('')
        if len(innerArray[1]) == 0:
            print("\x1b[6;30;47m Não existem testes marcados \x1b[0m")
        else:
            for elemento in range(len(innerArray[1])): #Posição dos Elementos de avaliação
                #print(innerArray[2][disciplina][elemento]) #iterar pela quantidade de Elementos de avaliação que existe na disciplina
                print('\x1b[6;30;102m {0:10} \x1b[0m'.format(innerArray[1][elemento]), end='|')
            print('\n')
            for data in range(len(innerArray[3])):
                # print(innerArray[4][disciplina][data])
                print('\x1b[6;30;47m {0:10} \x1b[0m'.format(innerArray[3][data]), end='|')
            print('\n')

def verDisciplina(matriz, indiceDisciplina, verClassificacoes=True): #imprime tabela completa sobre disciplina
    print("\x1b[44m Disciplina \x1b[0m|\x1b[46m Docente(s) \x1b[0m")
    print()
    print('\x1b[44m {0:10} \x1b[0m'.format(matriz[0][indiceDisciplina]), end='|')
    for docente in range(len(matriz[1][indiceDisciplina])):
        print('\x1b[46m {0:10} \x1b[0m'.format(matriz[1][indiceDisciplina][docente]), end='|')
    print('\n')
    for elemento in range(len(matriz[2][indiceDisciplina])):
        print('\x1b[6;30;102m {0:10} \x1b[0m'.format(matriz[2][indiceDisciplina][elemento]), end='|')
    print('\n')
    for ponderacao in range(len(matriz[3][indiceDisciplina])):
        print('\x1b[6;30;47m {0:10} % \x1b[0m'.format(matriz[3][indiceDisciplina][ponderacao]), end='|')
    print('\n')
    for data in range(len(matriz[4][indiceDisciplina])):
        print('\x1b[6;30;47m {0:10} \x1b[0m'.format(matriz[4][indiceDisciplina][data]), end='|')
    print('\n')
    if verClassificacoes:
        for classificacao in range(len(matriz[5][indiceDisciplina])):
            print('\x1b[6;30;47m {0:10} \x1b[0m'.format(matriz[5][indiceDisciplina][classificacao]), end='|')
        print('\n')
        calcularMedia(matriz[3][indiceDisciplina],matriz[5][indiceDisciplina])
    else:
        pass


#função para 1 matriz
def tabelaDisciplina(matriz): #imprimir disciplinas
    for i in range(len(matriz)):
        if i == 0:
            print('-'*len(matriz[i]*20))
            print("Lista de Disciplinas")
            print('-'*len(matriz[i]*20))
            print('\x1b[44m {0:10} \x1b[0m'.format(matriz[i]), end=' | ')
           # print('\n')
        else:
            print('\x1b[44m {0:10} \x1b[0m'.format(matriz[i]), end=' | ')
            #print('\n')

def tabelaDocentes(matriz): #imprimir docentes e disciplina
    print('-'*len(matriz[1]*20))
    print("Lista de Docentes")
    print('-'*len(matriz[1]*20))
    for disciplina in range(len(matriz[0])):
        print('\x1b[44m {0:10} \x1b[0m'.format(matriz[0][disciplina]), end=' | ')
        print('\n')
        for docente in range(len(matriz[1][disciplina])):
            print('\x1b[6;30;47m {0:10} \x1b[0m'.format(matriz[1][disciplina][docente]), end=' | ')
        print('\n')



""" ======= """
""" Helpers """
""" ======= """

#Funções de ajuda
def adicionarDisciplina(disciplina):
    disciplinas.append(disciplina)

def adicionarDocente(docente):
    docentes.append(docente)

def adicionarElemento(elemento):
    elementos.append(elemento)

def adicionarPonderacao(ponderacao):
    ponderacoes.append(ponderacao)

def adicionarData(data):
    datas.append(data)

def adicionarNotas(nota):
    notas.append(nota)

def adicionarPonderacaoNota(ponderacao,nota):
    ponderacaoNota.append(ponderacao)
    ponderacaoNota.append(nota)

# def adicionarAvaliacao(elemento_avaliacao, ponderacao, data, classificacao):
#     avaliacao.append(elemento_avaliacao)
#     avaliacao.append(ponderacao)
#     avaliacao.append(data)
#     avaliacao.append(classificacao)

# def adicionarClassificacao(elemento_avaliacao, classificacao):
#     elementos_avaliacao.append(elemento_avaliacao)
#     classificacoes.append(classificacao)

def escolherDisciplina(matriz):
    print('-'*len(matriz*20))
    print("Lista de Disciplinas")
    print('-'*len(matriz*20))
    print("Escolha um dos números referente à disciplina:")
    for i in range(len(matriz)):
        print('\x1b[7;43;43m {} \x1b[0m \x1b[44m {} \x1b[0m'.format(i,matriz[i]))


""" ================ """
""" Popular matrizes """
""" ================ """

#Função para criar docentes
def criarDocentes():
    ## Adicionar docentes.. começamos por perguntar se existe mais do que 1
    print("Existem mais do que 1 docente para a disciplina? [1]Sim [2]Não")
    opcao = int(input())

    if opcao == 1:
        docenteTrigger = False
        i = 1
        listaTempDeDocentes = []
        while docenteTrigger == False:
            print("Nome do {}º docente".format(i))
            listaTempDeDocentes.append(input())
            i += 1
            if i >= 3:
                print("Deseja adicionar outro docente? [1]Sim [2]Não")
                opcao = int(input())
                if opcao == 2:
                    adicionarDocente(listaTempDeDocentes)
                    docenteTrigger = True

    # Se só tiver um docente
    elif opcao == 2:
        print("Quem é o docente?")
        docente = [input()]
        adicionarDocente(docente)

#Função para criar elementos de avaliação
def criarElementosAvalicao():
    print("Quantos elementos de avaliação tem a disciplina?")
    n = int(input())
    elCounter = 1
    listaElementos = []
    listaPonderacoes = []
    listaDatas = []
    listaNotas = []

    listaPonderacaoComNota = []
    listaSoNota = []
    while elCounter <= n:
        print("{}º elemento:".format(elCounter))
        elemento = input()
        listaElementos.append(elemento)

        #Valida que é um número e que está entre 0 e 100 ..
        #Temos de validar que a ponderacao do 2º elemento não pode ser um numero que em conjunto com este ultrapasse os 100%
        pond_trigger = False
        while pond_trigger == False:
            pond = ''
            while pond is not int:
                try:
                    print("Ponderação do {}º elemento (de 0 a 100, não é necessário colocar %):".format(elCounter))
                    pond = int(input())
                    break
                except ValueError:
                    print("Insira um número")

            if ((pond >= 0) and (pond <= 100)):
                listaPonderacoes.append(pond)
                pond_trigger = True
            else:
                print("Erro! Inseriu um número menor que 0 ou maior que 100.")

        #Validação de data
        isValidDate = False
        while isValidDate == False:
            print("Data do {}º elemento, Insira no formato 'dd/mm/aa':".format(elCounter))
            inputDate = input()
            if len(inputDate) > 8 or len(inputDate) < 8:
                print ("Formato data deve ser: dd/mm/yy")
                isValidDate = False
            else:
                try :
                    day,month,year = inputDate.split('/')
                    datetime(int(year),int(month),int(day))
                    listaDatas.append(inputDate)
                    isValidDate = True
                except ValueError :
                    print("Insira no formato correcto")
                    isValidDate = False

        print("Já teve avaliação neste elemento? [1]Sim [2]Não")
        opcao_el = int(input())
        if opcao_el == 1:
            print("Qual a classificação que obteve?")
            nota_trigger = False
            while nota_trigger == False:
                nota = ''
                while nota is not int:
                    try:
                        print("Insira de 0 a 20:")
                        nota = int(input())
                        break
                    except ValueError:
                        print("Insira um número")
                if ((nota >= 0) and (nota <= 20)):
                    # elComNota.append(el)
                    listaNotas.append(nota)
                    listaPonderacaoComNota.append(pond)
                    listaSoNota.append(nota)
                    nota_trigger = True
                else:
                    print("Erro! Inseriu um número menor que 0 ou maior que 20.")
        else:
            listaNotas.append(" ")
            pass
        elCounter += 1
    adicionarElemento(listaElementos)
    adicionarPonderacao(listaPonderacoes)
    adicionarData(listaDatas)
    adicionarNotas(listaNotas)
    adicionarPonderacaoNota(listaPonderacaoComNota,listaSoNota)

#Função para criar disciplina
def criarDisciplina():
    ##Enquanto não escolher sair do programa... Loop
    trigger = False
    while trigger == False:
        ## Inserir disciplina
        print("Inserir disciplina")
        disciplina = input()
        adicionarDisciplina(disciplina)

        ##Funcoes
        criarDocentes()
        criarElementosAvalicao()

        ## Adicionar outra disciplina?
        print("Deseja adicionar outra disciplina? [1]Sim [2]Não")
        opcao = int(input())
        if opcao == 2:
            trigger = True
        else:
            pass


""" ================== """
""" Menu de utilização """
""" ================== """
## Criar um menu de opçoes para iniciar o programa
def menuPrincipal():
    op1 = int(input(
    '''
GAU "Gestão de Avaliação na UAL"
Escolha uma das seguintes funcionalidades:
[1] - Consultar
[2] - Registar
[3] - Carregar Dados
[0] - Sair

Opção: '''))

    if op1 == 1:
        menuSecundario()
    elif op1 == 2:
        criarDisciplina()
        menuPrincipal()
    elif op1 == 3:
        opcaoCarregar = int(input("Deseja ler os dados txt [1] ou carregar os dados bin [2]? \n"))
        if opcaoCarregar == 1:
            leTxt()
            print("Dados lidos com sucesso!")
            input("Prima qualquer tecla para voltar atrás.")
            menuSecundario()
        if opcaoCarregar == 2:
            leBin()
            print("Dados carregados com sucesso!")
            input("Prima qualquer tecla para voltar atrás.")
            menuSecundario()
    elif op1 == 0:
        print ('\x1b[1;37;44m' + "O programa irá encerrar" + '\x1b[0m')
        exit
    else:
        print ("Opção incorrecta")
        menuPrincipal()

def menuSecundario():
    op2 = int(input(
    '''
GAU "Gestão de Avaliação na UAL"
Escolha uma das seguintes funcionalidades que pretende consultar:

[1] - Consultar Curso
[2] - Lista de Disciplinas
[3] - Docentes
[4] - Elementos de Avaliação
[5] - Classificações Obtidas
[6] - Datas de Testes/Trabalhos
[7] - Media de disciplinas
[8] - Gravar dados
[9] - Grafico notas
[10] - Voltar menu anterior
[0] - Sair

Opção: '''))
    if op2 == 1:
        print ('\x1b[6;30;42m' + curso + '\x1b[0m')
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 2:
        tabelaDisciplina(megaArray[0])
        print('\n')
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 3:
        tabelaDocentes(megaArray)
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 4:
        escolherDisciplina(megaArray[0])
        indiceDisc = int(input("Escolha a disciplina:"))
        verDisciplina(megaArray,indiceDisc,False)
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 5:
        escolherDisciplina(megaArray[0])
        indiceDisc = int(input("Escolha a disciplina:"))
        verDisciplina(megaArray,indiceDisc)
        grafico(megaArray[5][indiceDisc], megaArray[2][indiceDisc])
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 6:
        escolherDisciplina(megaArray[0])
        indiceDisc = int(input("Escolha a disciplina:"))
        verProximasDatas(megaArray, indiceDisc)
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 7:
        escolherDisciplina(megaArray[0])
        indiceDisc = int(input("Escolha a disciplina:"))
        calcularNotaMinima(ponderacaoNota,indiceDisc)
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 8:
        inputOpcao = int(input("Gravar em txt [1] ou Gravar em binário [2] \n"))
        if inputOpcao == 1:
            gravaTxt(megaArray)
            print("Gravado com sucesso")
            input("Prima qualquer tecla para voltar atrás.")
            menuSecundario()
        elif inputOpcao == 2:
            gravaBin(megaArray)
            print("Gravado com sucesso")
            input("Prima qualquer tecla para voltar atrás.")
            menuSecundario()
        else:
            menuSecundario()
    elif op2 == 9:
        grafNotasFinais(notas,disciplinas)
        input("Prima qualquer tecla para voltar atrás.")
        menuSecundario()
    elif op2 == 10:
        input("Prima qualquer tecla para voltar atrás.")
        menuPrincipal()
    elif op2 == 0:
        print ("O programa irá encerrar")
        exit
    else:
        print ("Opção incorrecta")
        menuSecundario()

def menuSaida():
    op3 = int(input(
    '''
GAU "Gestão de Avaliação na UAL"

[1] - Menu Inicial
[0] - Sair

Opção: '''))
    if op3 == 1:
        menuPrincipal()
    elif op3 == 0:
        print ("O programa irá encerrar")
        exit
    else:
        print ("Opção incorrecta")
        menuSaida()


##com isto conseguimos importar apenas as funções criadas acima noutro ficheiro sem correr o ficheiro
if __name__ == "__main__":
    menuPrincipal()
